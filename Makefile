build:
	docker-compose build

down:
	docker-compose down

roman:
	docker-compose run --rm katas pytest -k 'roman' -p no:cacheprovider -vs

potter:
	docker-compose run --rm katas pytest -k 'potter' -p no:cacheprovider -vs

main:
	docker-compose run --rm katas python main.py

shell:
	docker-compose run --rm katas bash

clear:
	docker-compose down && \
	docker container prune -f && \
	docker image prune -f

ci-test:
	docker-compose run --rm katas pytest
