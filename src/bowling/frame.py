from src.bowling.IllegalRollError import IllegalRollError


class Frame:
    def __init__(self):
        self.rolls = 0
        self.score = 0

    def roll(self, pins):
        self.rolls += 1
        self._check_rolls()
        self.score += pins

    def calculate_score(self):
        return self.score

    def _check_rolls(self):
        if self.rolls >= 3:
            raise IllegalRollError
