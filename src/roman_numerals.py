class RomanNumerals:
    SUBSTRACTIVES = [100, 10, 1]
    EQUIVALENCES = {
        1000: 'M',
        500: 'D',
        100: 'C',
        50: 'L',
        10: 'X',
        5: 'V',
        1: 'I',
    }

    @classmethod
    def romanize(cls, arabic, accumulator=''):
        if arabic == 0: return accumulator

        roman_data = cls._find_suitable_roman(arabic)
        roman_symbol = roman_data['roman_symbol']
        arabic_value = roman_data['arabic_value']

        return cls.romanize(arabic - arabic_value, accumulator + roman_symbol)


    @classmethod
    def _find_suitable_roman(cls, arabic):
        arabicAsString = str(arabic)
        sign = 1
        suitables = cls.EQUIVALENCES.keys()

        if arabicAsString.startswith('4') or arabicAsString.startswith('9'):
            sign = -1
            suitables = cls.SUBSTRACTIVES

        findGenerator = (e for e in suitables if e <= arabic)
        suitable = next(findGenerator)

        return { 'arabic_value': suitable * sign, 'roman_symbol': cls.EQUIVALENCES[suitable] }
