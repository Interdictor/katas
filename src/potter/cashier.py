from copy import deepcopy

class Cashier:
    BASE_PRICE = 8
    DEFAULT_MAX_DISCOUNT_GROUP_SIZE = 5
    DISCOUNTS = {
        1: 1,
        2: 0.95,
        3: 0.9,
        4: 0.8,
        5: 0.75,
    }

    def calculate_price(self, basket):
        data = self._generate_many_groups_data(basket)

        prices = []
        for discount_groups in data:
            price = sum(self._calculate_discounted_price(dg) for dg in discount_groups)
            prices.append(price)

        return min(prices)

    def _generate_many_groups_data(self, basket):
        accumulator = [[]]
        group_data = self._group_discounts_recursively(
            deepcopy(basket),
            accumulator,
        )

        result = [group_data['discount_groups']]

        biggest_group_size = group_data['biggest_group_size']
        if biggest_group_size > 2:
            accumulator = [[]]
            alternative_group_data = self._group_discounts_recursively(
                deepcopy(basket),
                accumulator,
                biggest_group_size - 1,
            )
            result.append(alternative_group_data['discount_groups'])

        return result

    def _group_discounts_recursively(
        self,
        books,
        accumulator,
        max_group_size=DEFAULT_MAX_DISCOUNT_GROUP_SIZE
    ):

        if len(books) == 0:
            biggest_group_size = self._determine_biggest_size(accumulator)
            return {
                'discount_groups': accumulator,
                'biggest_group_size': biggest_group_size,
            }

        book = books.pop()
        for discount_group in accumulator:
            current_group_max_out = len(discount_group) >= max_group_size
            last_group_is_maxed_out = len(accumulator[-1]) >= max_group_size

            if book in accumulator[-1] or last_group_is_maxed_out:
                accumulator.append([])

            # if current_group_max_out and book in accumulator[-1]:
            # jugando con esto me di cuenta de que no hacía falta esta condición
            #     # accumulator.append([])
            #     continue

            if (not book in discount_group) and not current_group_max_out:
                discount_group.append(book)
                return self._group_discounts_recursively(
                    books,
                    accumulator,
                    max_group_size
                )

    def _calculate_discounted_price(self, discount_group):
        books_amount = len(discount_group)
        return self.BASE_PRICE * books_amount * self.DISCOUNTS[books_amount]

    def _determine_biggest_size(self, discount_groups):
        sizes = [len(group) for group in discount_groups]
        return max(sizes)
