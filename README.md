# INTERDICTOR KATAS

## REQUERIMENTS

* Docker 20.10.8 or superior
* docker-compose 1.26.2 or superior
* GNU Make 4.2.1 or superior

## COMMANDS

* launch roman_numeral tests: `make roman`

For additional commands see Makefile in root folder.

## HOW TO?

* skip a test, decorate it with: `@pytest.mark.skip()`
