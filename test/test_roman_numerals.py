import pytest
from src.roman_numerals import RomanNumerals

class TestRomanNumerals:
    def test_it_translates_arabic_unit_to_roman_unit(self):
        arabic = 1

        result = RomanNumerals.romanize(arabic)

        assert result == 'I'

    def test_it_translates_roman_numbers_with_the_same_symbol_repeated(self):
        arabic = 3

        result = RomanNumerals.romanize(arabic)

        assert result == 'III'

    def test_it_translates_arabic_five(self):
        arabic = 5

        result = RomanNumerals.romanize(arabic)

        assert result == 'V'

    def test_it_romanizes_four(self):
        expected_result = 'IV'
        arabic = 4

        result = RomanNumerals.romanize(arabic)

        assert result == expected_result


    def test_it_handles_additive_symbols(self):
        scenarios = [
            { 'arabic': 6, 'expected_result': 'VI' },
            { 'arabic': 23, 'expected_result': 'XXIII' },
            { 'arabic': 53, 'expected_result': 'LIII' },
            { 'arabic': 106, 'expected_result': 'CVI' },
            { 'arabic': 551, 'expected_result': 'DLI' },
            { 'arabic': 2021, 'expected_result': 'MMXXI' },
        ]

        for scenario in scenarios:
            result = RomanNumerals.romanize(scenario['arabic'])

            assert result == scenario['expected_result']

    def test_it_handles_substractive_symbols(self):
        scenarios = [
            { 'arabic': 4, 'expected_result': 'IV' },
            { 'arabic': 39, 'expected_result': 'XXXIX' },
            { 'arabic': 43, 'expected_result': 'XLIII' },
            { 'arabic': 99, 'expected_result': 'XCIX' },
            { 'arabic': 490, 'expected_result': 'CDXC' },
            { 'arabic': 1991, 'expected_result': 'MCMXCI' },
        ]

        for scenario in scenarios:
            result = RomanNumerals.romanize(scenario['arabic'])

            assert result == scenario['expected_result']
