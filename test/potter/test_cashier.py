from src.potter.cashier import Cashier
import pytest


BASE_BOOK_PRICE = 8
DISCOUNT_FOR_TWO = 0.95
DISCOUNT_FOR_THREE = 0.9
DISCOUNT_FOR_FOUR = 0.80
DISCOUNT_FOR_FIVE = 0.75


class TestPotter:
    def test_single_book(self):
        basket = ['a']
        cashier = Cashier()

        result = cashier.calculate_price(basket)

        assert result == BASE_BOOK_PRICE

    def test_it_handles_many_equal_books(self):
        basket = ['a', 'a', 'a']
        cashier = Cashier()

        result = cashier.calculate_price(basket)

        assert result == BASE_BOOK_PRICE * 3

    def test_it_handles_discount_groups(self):
        basket = ['a', 'b']
        cashier = Cashier()

        result = cashier.calculate_price(basket)

        assert result == BASE_BOOK_PRICE * 2 * DISCOUNT_FOR_TWO

    def test_it_applies_4_different_books_discount(self):
        basket = ['a', 'b', 'c', 'd']
        cashier = Cashier()

        result = cashier.calculate_price(basket)

        assert result == BASE_BOOK_PRICE * len(basket) * DISCOUNT_FOR_FOUR

    def test_it_handles_mixed_simple_situations(self):
        basket = ['a', 'b', 'c', 'c']
        cashier = Cashier()

        result = cashier.calculate_price(basket)

        assert result == float(BASE_BOOK_PRICE * 3 * DISCOUNT_FOR_THREE) + BASE_BOOK_PRICE

    def test_it_handles_two_different_discount_groups(self):
        basket = ['a', 'b', 'c', 'd', 'e', 'a', 'b']
        cashier = Cashier()

        result = cashier.calculate_price(basket)

        assert result == (5 * DISCOUNT_FOR_FIVE + 2 * DISCOUNT_FOR_TWO) * BASE_BOOK_PRICE

    def test_it_handles_unsorted_baskets(self):
        basket = ['b', 'b', 'a', 'a']
        cashier = Cashier()

        result = cashier.calculate_price(basket)

        assert result == len(basket) * DISCOUNT_FOR_TWO * BASE_BOOK_PRICE

    def test_it_select_the_best_discounts(self):
        basket = ['a', 'b', 'c', 'd', 'e', 'a', 'b', 'c']
        cashier = Cashier()

        result = cashier.calculate_price(basket)

        assert result == (len(basket) * BASE_BOOK_PRICE * DISCOUNT_FOR_FOUR)

    def test_it_handles_complex_situations(self):
        basket = ['a', 'b', 'c', 'd', 'e', 'a', 'b', 'c', 'd', 'e', 'a', 'b']
        cashier = Cashier()

        result = cashier.calculate_price(basket)

        assert result == (10 * DISCOUNT_FOR_FIVE + 2 * DISCOUNT_FOR_TWO) * BASE_BOOK_PRICE
