import pytest # type: ignore
from src.bowling.IllegalRollError import IllegalRollError
from src.bowling.frame import Frame


class TestFrame:
    def test_it_scores_two_times_no_pins_down(self):
        frame = Frame()

        frame.roll(0)
        frame.roll(0)
        result = frame.calculate_score()

        assert result == 0

    def test_it_scores_one_point(self):
        frame = Frame()

        frame.roll(1)
        frame.roll(0)
        result = frame.calculate_score()

        assert result == 1

    def test_it_cannot_rolls_thrice(self):
        frame = Frame()

        with pytest.raises(IllegalRollError):
            frame.roll(1)
            frame.roll(0)
            frame.roll(0)
